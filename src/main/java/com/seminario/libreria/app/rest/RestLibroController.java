package com.seminario.libreria.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.seminario.libreria.app.model.Libro;
import com.seminario.libreria.app.repo.ILibroRepo;

import io.swagger.annotations.ApiOperation;;

@RestController
@RequestMapping("/libros")
public class RestLibroController {

	@Autowired
	private ILibroRepo repo;
	
	@GetMapping
	@ApiOperation(value = "Listado de Libros del sistema", response = Libro.class)
	public List<Libro> listar(){
		return repo.findAll();
	}
	
	@PostMapping("/create")
	@ApiOperation(value = "Crear un nuevo libro")
	public ResponseEntity<Libro> insertar(@RequestBody Libro libro){
		
		repo.save(libro);
		
		return new ResponseEntity<>(
				libro, 
		      HttpStatus.OK);
	}
	
	@PutMapping("/update")
	@ApiOperation(value = "Actualizar un Usuario")
	public ResponseEntity<Libro> modificar(@RequestBody Libro libro){
		repo.save(libro);
		
		return new ResponseEntity<>(
				libro, 
		      HttpStatus.OK);
	}
}
