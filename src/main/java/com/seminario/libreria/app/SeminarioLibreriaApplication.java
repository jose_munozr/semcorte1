package com.seminario.libreria.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeminarioLibreriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeminarioLibreriaApplication.class, args);
	}

}
