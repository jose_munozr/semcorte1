package com.seminario.libreria.app.repo;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.seminario.libreria.app.model.Libro;

@Repository
public interface ILibroRepo extends JpaRepository<Libro, Integer>{
	Libro findById(int id);
}
